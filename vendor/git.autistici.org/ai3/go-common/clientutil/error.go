package clientutil

import (
	"fmt"
	"io/ioutil"
	"net/http"
)

// RemoteError represents a HTTP error from the server. The status
// code and response body can be retrieved with the StatusCode() and
// Body() methods.
type RemoteError struct {
	statusCode int
	body       string
}

func remoteErrorFromResponse(resp *http.Response) *RemoteError {
	// Optimistically read the response body, ignoring errors.
	var body string
	if data, err := ioutil.ReadAll(resp.Body); err == nil {
		body = string(data)
	}
	return &RemoteError{statusCode: resp.StatusCode, body: body}
}

// Error implements the error interface.
func (e *RemoteError) Error() string {
	return fmt.Sprintf("%d - %s", e.statusCode, e.body)
}

// StatusCode returns the HTTP status code.
func (e *RemoteError) StatusCode() int { return e.statusCode }

// Body returns the response body.
func (e *RemoteError) Body() string { return e.body }
