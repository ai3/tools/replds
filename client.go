package replds

import (
	"context"

	"git.autistici.org/ai3/go-common/clientutil"
)

// PublicClient for the public HTTP API.
type PublicClient interface {
	SetNodes(context.Context, *SetNodesRequest) (*SetNodesResponse, error)
}

// NewPublicClient returns a new client for the public HTTP API.
func NewPublicClient(config *clientutil.BackendConfig) (PublicClient, error) {
	return newClient(config)
}

// An internal client (usually bound to a specific peer).
type client struct {
	be clientutil.Backend
}

func newClient(config *clientutil.BackendConfig) (*client, error) {
	be, err := clientutil.NewBackend(config)
	if err != nil {
		return nil, err
	}
	return &client{be}, nil
}

func (c *client) internalGetNodes(ctx context.Context, req *internalGetNodesRequest) (*internalGetNodesResponse, error) {
	var resp internalGetNodesResponse
	err := c.be.Call(ctx, "", "/api/internal/get_nodes", req, &resp)
	return &resp, err
}

func (c *client) internalUpdateNodes(ctx context.Context, req *internalUpdateNodesRequest) error {
	return c.be.Call(ctx, "", "/api/internal/update_nodes", req, nil)
}

func (c *client) SetNodes(ctx context.Context, req *SetNodesRequest) (*SetNodesResponse, error) {
	var resp SetNodesResponse
	err := c.be.Call(ctx, "", "/api/set_nodes", req, &resp)
	return &resp, err
}

type network struct {
	peers map[string]*client
}

func newNetwork(peers []string, tlsConfig *clientutil.TLSClientConfig) (*network, error) {
	peerMap := make(map[string]*client)
	for _, peer := range peers {
		client, err := newClient(peerConfig(peer, tlsConfig))
		if err != nil {
			return nil, err
		}
		peerMap[peer] = client
	}
	return &network{peers: peerMap}, nil
}

func (n *network) Client(peer string) *client {
	return n.peers[peer]
}

func peerConfig(peer string, tlsConfig *clientutil.TLSClientConfig) *clientutil.BackendConfig {
	return &clientutil.BackendConfig{
		URL:       peer,
		TLSConfig: tlsConfig,
	}
}
