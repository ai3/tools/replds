module git.autistici.org/ai3/tools/replds

go 1.14

require (
	git.autistici.org/ai3/go-common v0.0.0-20230816213645-b3aa3fb514d6
	github.com/coreos/go-systemd/v22 v22.5.0
	github.com/google/subcommands v1.2.0
	github.com/prometheus/client_golang v1.12.2
	gopkg.in/yaml.v3 v3.0.1
)
