package main

import (
	"context"
	"flag"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"time"

	"github.com/google/subcommands"

	"git.autistici.org/ai3/tools/replds"
)

func nodeFromFile(path, rootDir string) (*replds.Node, error) {
	stat, err := os.Stat(path)
	if err != nil {
		return nil, err
	}
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}

	rel := path
	if rootDir != "" {
		rel, err = filepath.Rel(rootDir, path)
		if err != nil {
			return nil, err
		}
	}

	return &replds.Node{
		Path:      rel,
		Value:     data,
		Timestamp: stat.ModTime(),
	}, nil
}

type putCmd struct {
	root string
}

func (*putCmd) Name() string     { return "put" }
func (*putCmd) Synopsis() string { return "Upload one or more files." }
func (*putCmd) Usage() string {
	return `put [<OPTIONS>] <FILE>...
  Upload one or more files.

`
}

func (c *putCmd) SetFlags(f *flag.FlagSet) {
	f.StringVar(&c.root, "relative-to", "", "set base `path` for files")
}

func (c *putCmd) Execute(ctx context.Context, f *flag.FlagSet, _ ...interface{}) subcommands.ExitStatus {
	if f.NArg() == 0 {
		log.Printf("not enough arguments")
		return subcommands.ExitUsageError
	}

	config, err := loadConfig()
	if err != nil {
		log.Printf("error loading config: %v", err)
		return subcommands.ExitFailure
	}

	client, err := replds.NewPublicClient(config.Client)
	if err != nil {
		log.Printf("error creating client: %v", err)
		return subcommands.ExitFailure
	}

	rootDir := c.root
	if rootDir == "" {
		rootDir, err = os.Getwd()
		if err != nil {
			log.Printf("could not determine working directory: %v", err)
			return subcommands.ExitFailure
		}
	}

	// Build a single SetNodesRequest with all the data.
	var req replds.SetNodesRequest
	for _, arg := range f.Args() {
		node, err := nodeFromFile(arg, rootDir)
		if err != nil {
			log.Printf("error: %v", err)
			return subcommands.ExitFailure
		}
		req.Nodes = append(req.Nodes, node)
	}

	ctx, cancel := context.WithTimeout(ctx, 600*time.Second)
	defer cancel()

	resp, err := client.SetNodes(ctx, &req)
	if err != nil {
		log.Printf("error uploading data: %v", err)
		return subcommands.ExitFailure
	}
	log.Printf("data uploaded successfully (%d ok / %d errs)", resp.HostsOk, resp.HostsErr)
	return subcommands.ExitSuccess
}

type rmCmd struct{}

func (*rmCmd) Name() string     { return "rm" }
func (*rmCmd) Synopsis() string { return "Delete one or more files." }
func (*rmCmd) Usage() string {
	return `rm <FILE>...
  Delete one or more files.

`
}

func (c *rmCmd) SetFlags(f *flag.FlagSet) {}

func (c *rmCmd) Execute(_ context.Context, f *flag.FlagSet, _ ...interface{}) subcommands.ExitStatus {
	if f.NArg() == 0 {
		log.Printf("not enough arguments")
		return subcommands.ExitUsageError
	}

	config, err := loadConfig()
	if err != nil {
		log.Printf("error loading config: %v", err)
		return subcommands.ExitFailure
	}

	client, err := replds.NewPublicClient(config.Client)
	if err != nil {
		log.Printf("error creating client: %v", err)
		return subcommands.ExitFailure
	}

	now := time.Now()
	var req replds.SetNodesRequest
	for _, arg := range f.Args() {
		req.Nodes = append(req.Nodes, &replds.Node{
			Path:      arg,
			Deleted:   true,
			Timestamp: now,
		})
	}

	ctx, cancel := context.WithTimeout(context.Background(), 600*time.Second)
	defer cancel()

	resp, err := client.SetNodes(ctx, &req)
	if err != nil {
		log.Printf("error uploading data: %v", err)
		return subcommands.ExitFailure
	}
	log.Printf("data uploaded successfully (%d ok / %d errs)", resp.HostsOk, resp.HostsErr)
	return subcommands.ExitSuccess
}

type syncCmd struct {
	root string
	del  bool
}

func (*syncCmd) Name() string     { return "sync" }
func (*syncCmd) Synopsis() string { return "Sync a local path against the remote repository." }
func (*syncCmd) Usage() string {
	return `sync [<OPTIONS>] <PATH>
  Sync a local path against the remote repository.

`
}

func (c *syncCmd) SetFlags(f *flag.FlagSet) {
	f.StringVar(&c.root, "relative-to", "", "set base `path` for files")
	f.BoolVar(&c.del, "delete", false, "delete remote files")
}

func (c *syncCmd) scanPath(base string) map[string]time.Time {
	m := make(map[string]time.Time)
	if err := filepath.Walk(base, func(path string, info os.FileInfo, err error) error {
		if err != nil || !info.Mode().IsRegular() {
			return nil
		}
		if rel, rerr := filepath.Rel(base, path); rerr == nil {
			m[rel] = info.ModTime()
		}
		return nil
	}); err != nil {
		log.Printf("error: %v", err)
	}
	return m
}

func (c *syncCmd) diffScans(a, b map[string]time.Time) ([]string, []string) {
	var added, removed []string
	for bf, bts := range b {
		ats, ok := a[bf]
		if !ok || ats.Before(bts) {
			added = append(added, bf)
		} else {
			delete(a, bf)
		}
	}
	for af := range a {
		removed = append(removed, af)
	}
	return added, removed
}

func (c *syncCmd) Execute(_ context.Context, f *flag.FlagSet, _ ...interface{}) subcommands.ExitStatus {
	if f.NArg() != 1 {
		log.Printf("wrong number of arguments")
		return subcommands.ExitUsageError
	}

	config, err := loadConfig()
	if err != nil {
		log.Printf("error loading config: %v", err)
		return subcommands.ExitFailure
	}

	client, err := replds.NewPublicClient(config.Client)
	if err != nil {
		log.Printf("error creating client: %v", err)
		return subcommands.ExitFailure
	}

	ref := c.scanPath(config.Server.RootPath)
	src := c.scanPath(flag.Arg(0))
	added, removed := c.diffScans(ref, src)

	var req replds.SetNodesRequest
	for _, path := range added {
		node, err := nodeFromFile(path, "")
		if err != nil {
			continue
		}
		req.Nodes = append(req.Nodes, node)
	}
	if c.del {
		now := time.Now()
		for _, path := range removed {
			req.Nodes = append(req.Nodes, &replds.Node{
				Path:      path,
				Deleted:   true,
				Timestamp: now,
			})
		}
	}

	ctx, cancel := context.WithTimeout(context.Background(), 600*time.Second)
	defer cancel()

	resp, err := client.SetNodes(ctx, &req)
	if err != nil {
		log.Printf("error uploading data: %v", err)
		return subcommands.ExitFailure
	}
	log.Printf("data uploaded successfully (%d ok / %d errs)", resp.HostsOk, resp.HostsErr)
	return subcommands.ExitSuccess
}

func init() {
	subcommands.Register(&putCmd{}, "")
	subcommands.Register(&syncCmd{}, "")
	subcommands.Register(&rmCmd{}, "")
}
