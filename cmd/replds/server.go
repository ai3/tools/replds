package main

import (
	"context"
	"flag"
	"log"
	"os"
	"os/signal"
	"syscall"

	"git.autistici.org/ai3/go-common/serverutil"
	"github.com/coreos/go-systemd/v22/daemon"
	"github.com/google/subcommands"

	"git.autistici.org/ai3/tools/replds"
)

type serverCmd struct{}

func (*serverCmd) Name() string     { return "server" }
func (*serverCmd) Synopsis() string { return "Run the server." }
func (*serverCmd) Usage() string {
	return `server [<OPTIONS>]
  Run the server.

`
}

func (c *serverCmd) SetFlags(f *flag.FlagSet) {}

func (c *serverCmd) Execute(_ context.Context, f *flag.FlagSet, _ ...interface{}) subcommands.ExitStatus {
	if f.NArg() != 0 {
		log.Printf("too many arguments")
		return subcommands.ExitUsageError
	}

	config, err := loadConfig()
	if err != nil {
		log.Printf("error loading config: %v", err)
		return subcommands.ExitFailure
	}
	if config.Server.Addr == "" {
		log.Printf("configuration error: 'server.addr' is not set")
		return subcommands.ExitFailure
	}
	if config.Server.RootPath == "" {
		log.Printf("configuration error: 'server.path' is not set")
		return subcommands.ExitFailure
	}

	srv, err := replds.NewServer(
		config.Server.Peers,
		config.Server.RootPath,
		config.Server.TLSClientConfig,
		config.Server.Readonly,
	)
	if err != nil {
		log.Printf("error initializing server: %v", err)
		return subcommands.ExitFailure
	}
	defer srv.Close()

	// If this is a readonly server, just wait for a termination
	// signal, otherwise start the HTTP server for our API.
	if config.Server.Readonly {
		// We must notify systemd of our successful startup
		// (this is normally done by serverutil.Serve in the
		// non-readonly case).
		daemon.SdNotify(false, "READY=1") // nolint:errcheck

		sigCh := make(chan os.Signal, 1)
		signal.Notify(sigCh, syscall.SIGINT, syscall.SIGTERM)
		<-sigCh
	} else {
		httpSrv := replds.NewHTTPServer(srv)

		log.Printf("starting replds on %s", config.Server.Addr)
		if err := serverutil.Serve(httpSrv.Handler(), config.HTTPServer, config.Server.Addr); err != nil {
			log.Printf("error: %v", err)
			return subcommands.ExitFailure
		}
	}

	return subcommands.ExitSuccess
}

func init() {
	subcommands.Register(&serverCmd{}, "")
}
