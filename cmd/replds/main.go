package main

import (
	"context"
	"flag"
	"log"
	"os"

	"git.autistici.org/ai3/go-common/clientutil"
	"git.autistici.org/ai3/go-common/serverutil"
	"github.com/google/subcommands"
	"gopkg.in/yaml.v3"
)

var (
	configFile = flag.String("config", "/etc/replds/config.yml", "path of config file")
)

// Config is the configuration shared by all commands.
type Config struct {
	Client *clientutil.BackendConfig `yaml:"client"`
	Server struct {
		Addr            string                      `yaml:"addr"`
		RootPath        string                      `yaml:"path"`
		Peers           []string                    `yaml:"peers"`
		Readonly        bool                        `yaml:"readonly"`
		TLSClientConfig *clientutil.TLSClientConfig `yaml:"tls_client"`
	} `yaml:"server"`
	HTTPServer *serverutil.ServerConfig `yaml:"http_server"`
}

// Read the YAML configuration file.
func loadConfig() (*Config, error) {
	f, err := os.Open(*configFile)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	var config Config
	if err := yaml.NewDecoder(f).Decode(&config); err != nil {
		return nil, err
	}
	return &config, nil
}

func init() {
	subcommands.Register(subcommands.HelpCommand(), "")
	subcommands.Register(subcommands.FlagsCommand(), "")
	subcommands.Register(subcommands.CommandsCommand(), "")
}

func main() {
	log.SetFlags(0)
	flag.Parse()

	os.Exit(int(subcommands.Execute(context.Background())))
}
